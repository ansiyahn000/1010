Thanks for purchasing this product!
If you like it, please rate it!

Offline guide: Assets/FlexCompiler/Manual.pdf
Online guide: https://flexframework.net
Support: theoxuanx@gmail.com

I'd be thrilled to receive your requests/feedbacks/concerns!

IMPORTANT: 
for this exception "Unhandled Exception: System.Reflection.ReflectionTypeLoadException: The classes in the module cannot be loaded.",
please import the UnhandledExceptionFix.unitypackage

For Unity 2017.3 and later, please import 'UnhandledExceptionFix.unitypackage' if FlexCompiler failed to be loaded