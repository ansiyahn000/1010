﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BlockParent : MonoBehaviour,IEndDragHandler,IDragHandler, IPointerDownHandler,IPointerUpHandler
{
    public bool draggable;
    public bool placed;
    public BlockUnit[] units;
    bool canFit;
    Vector3 startPos;
    Vector3 localPos;

    Vector3 startScale = new Vector3(.65f, .65f, 65f);
    public List<int> ColoumnXs;
    public List<Slot> markerOnSlots;
    public List<Slot> markerOffSlots;
    public List<int> rowYs;
    public bool isGameOver;
    public Transform parentTransform;
    float scaleDuration = .1f;
    float moveDuration = .25f;
   // Ease easeType = Ease.Linear;
    float yPosOffset = 2f;

    public SpriteRenderer[] colourSprites;
    int blockUnitsCount;
    private void Awake()
    {
        parentTransform = transform.parent;
        colourSprites = GetComponentsInChildren<SpriteRenderer>();
        Input.multiTouchEnabled = false;
    }

    void Start()
    {

        units = GetComponentsInChildren<BlockUnit>();
        blockUnitsCount = units.Length;
        startPos = transform.position;
        localPos = transform.localPosition;

    }



    public void OnPointerUp(PointerEventData eventData)
    {
        StartCoroutine(CheckPlacing());
     //   OnMouseEnddrag();
    }

   


    IEnumerator CheckPlacing()
    {
        yield return new WaitForSeconds(.15f);
        if (!placed)
        {
            parentTransform.localScale = startScale;
            transform.localPosition = localPos;
            foreach (AudioSource source in BlockSnatch.instance.audioSources)
            {
                if (!source.isPlaying)
                {
                    source.PlayOneShot(BlockSnatch.instance.incorrectPlacementSFX);
                    break;
                }
            }
        }
        foreach (SpriteRenderer sprite in colourSprites)
        {
            if(sprite!=null)
            {
                sprite.sortingOrder = 10;
            }
            
        }
        draggable=false;
    }

    //  void OnMouseDown()
    public void OnPointerDown(PointerEventData eventData)  
    {
        if (!placed)
        {
            StartCoroutine(ScaleBlocks(Vector3.one));
            foreach (AudioSource source in BlockSnatch.instance.audioSources)
            {
                if (!source.isPlaying)
                {
                    source.PlayOneShot(BlockSnatch.instance.pickingSFX);
                    break;
                }
            }
        }
        foreach (SpriteRenderer sprite in colourSprites)
        {
            sprite.sortingOrder = 50;
        }
    }

   
    IEnumerator ScaleBlocks(Vector3 finalSize)
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        StartCoroutine(ScaleFunction(parentTransform, finalSize, parentTransform.localScale, scaleDuration));
        Vector3 endPos = new Vector3(mousePos.x, mousePos.y + yPosOffset, 0f);
        StartCoroutine(MoveFunction(this.transform, endPos, transform.position, scaleDuration));
        yield return new WaitForSeconds(scaleDuration);
        draggable = true;
    }

    IEnumerator MoveBlocks(Vector3 finalPos, float duration)
    {
        StartCoroutine(MoveFunction(this.transform, finalPos, transform.position, duration));
        yield return new WaitForSeconds(duration);
    
    }

  

      void IEndDragHandler.OnEndDrag(PointerEventData eventData)
   //  void OnMouseEnddrag()
    {
        
        if (draggable)
        {
            int i = 0;
            foreach (BlockUnit unit in units)
            {
                int indexX = Mathf.RoundToInt(unit.transform.position.x);
                int indexY = Mathf.RoundToInt(unit.transform.position.y);
                if (indexX < 10 && indexY < 10)
                {
                    if (indexX >= 0 && indexY >= 0)
                    {

                        if (BlockSnatch.instance.gameSlots[indexX].slots[indexY].empty)
                        {
                            i++;
                        }

                    }
                }




            }

            if (i == units.Length)
            {

                draggable = false;
                transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), 0f);
                placed = true;
                foreach (AudioSource source in BlockSnatch.instance.audioSources)
                {
                    if (!source.isPlaying)
                    {
                        source.PlayOneShot(BlockSnatch.instance.placingSFX);
                        break;
                    }
                }


                foreach (BlockUnit unit in units)
                {
                    int indexX = Mathf.RoundToInt(unit.transform.position.x);
                    int indexY = Mathf.RoundToInt(unit.transform.position.y);
                    BlockSnatch.instance.gameSlots[indexX].slots[indexY].empty = false;
                    BlockSnatch.instance.gameSlots[indexX].slots[indexY].blockUnit = unit;
                }
                BlockSnatch.instance.blocksCount -= 1;

                if (BlockSnatch.instance.blocksCount == 0)
                {
                    BlockSnatch.instance.blocksSpawned = false;
                    BlockSnatch.instance.BlockSpawner();
                    BlockSnatch.instance.blocksCount = 3;
                }
               
            }
           

        }


        //Coloumn wise checking
        for (int j = 0; j < 10; j++)
        {
            int occupiedSlotsValue = 0;
            for (int z = 0; z < 10; z++)
            {
                if (!BlockSnatch.instance.gameSlots[j].slots[z].empty)
                {
                    occupiedSlotsValue += 1;
                }
            }


            if (occupiedSlotsValue == 10)
            {
                ColoumnXs.Add(j);

                
            }


        }

        //Row wise checking
        for (int j = 0; j < 10; j++)
        {
            int occupiedSlotsValue = 0;
            for (int z = 0; z < 10; z++)
            {
                if (!BlockSnatch.instance.gameSlots[z].slots[j].empty)
                {
                    occupiedSlotsValue += 1;
                }
            }


            if (occupiedSlotsValue == 10)
            {
                rowYs.Add(j);               
            }
        }
       
        StartCoroutine(ColoumnDestruction());

        StartCoroutine(RowDestruction());
        BlockSnatch.instance.SnatchCountDigit.text = (ColoumnXs.Count + rowYs.Count).ToString();
        BlockSnatch.instance.SnatchCountDigitShadow.text = (ColoumnXs.Count + rowYs.Count).ToString();
        BlockSnatch.instance.scoreUpdater(ColoumnXs.Count + rowYs.Count);
        

        for (int j = 0; j < 10; j++)
        {

            for (int z = 0; z < 10; z++)
            {
                BlockSnatch.instance.gameSlots[j].slots[z].MarkerObject.SetActive(false);
            }
        }

        StartCoroutine(EmptySlotChecking());

     

    }

    IEnumerator RowDestruction()
    {
        foreach (int rowY in rowYs)
        {
            StartCoroutine(RowDestroy(rowY));
        } 
        yield return new WaitForSeconds(0f);
    }

    IEnumerator RowDestroy(int rowY)
    {
    //    BlockSnatch.instance.scoreUpdater();
        foreach (AudioSource source in BlockSnatch.instance.audioSources)
        {
            if (!source.isPlaying)
            {
                source.PlayOneShot(BlockSnatch.instance.rowCompletedSFX);
                break;
            }
        }
        for (int z = 0; z < 10; z++)
        {          
            if (BlockSnatch.instance.gameSlots[z].slots[rowY].blockUnit != null)
            {
                StartCoroutine(ScaleFunction(BlockSnatch.instance.gameSlots[z].slots[rowY].blockUnit.transform, Vector3.zero, Vector3.one,.25f));
               // BlockSnatch.instance.gameSlots[z].slots[rowY].blockUnit.transform.DOScale(Vector3.zero, .15f).SetEase(Ease.InElastic);
                BlockSnatch.instance.gameSlots[z].slots[rowY].blockUnit = null;
            }
             yield return new WaitForSeconds(.025f);
             BlockSnatch.instance.gameSlots[z].slots[rowY].empty = true;

        }
    }

    IEnumerator ColoumnDestruction()
    {
        foreach (int ColoumnX in ColoumnXs)
        {
            {
                StartCoroutine(ColoumnDestroy(ColoumnX));
              
            }

        }
        yield return new WaitForSeconds(0f);
    }

    IEnumerator ColoumnDestroy(int ColoumnX)
    {
      //  BlockSnatch.instance.scoreUpdater();
        foreach (AudioSource source in BlockSnatch.instance.audioSources)
        {
            if (!source.isPlaying)
            {
                source.PlayOneShot(BlockSnatch.instance.rowCompletedSFX);
                break;
            }
        }
        for (int k = 9; k>=0; k--)
        {
            
            if (BlockSnatch.instance.gameSlots[ColoumnX].slots[k].blockUnit != null)
            {
               
               StartCoroutine(ScaleFunction(BlockSnatch.instance.gameSlots[ColoumnX].slots[k].blockUnit.transform,Vector3.zero,Vector3.one,.25f));
                
            }
             yield return new WaitForSeconds(.025f);
            BlockSnatch.instance.gameSlots[ColoumnX].slots[k].empty = true;
            BlockSnatch.instance.gameSlots[ColoumnX].slots[k].blockUnit = null;
        }
    }
    IEnumerator EmptySlotChecking()
    {
        yield return new WaitForSeconds(.5f);
        {
            if (BlockSnatch.instance.blocksSpawned)
            {
                if (BlockSnatch.instance.currentBlockList != null)
                {
                    List<BlockParent> BlockParentblocks = new List<BlockParent>();
                    foreach (GameObject block in BlockSnatch.instance.currentBlockList)
                    {
                        BlockParentblocks.Add(block.GetComponentInChildren<BlockParent>());
                    }

                    foreach (BlockParent currentBlock in BlockParentblocks)

                    {
                        if (currentBlock.placed == false)
                        {

                            Debug.ClearDeveloperConsole();
                            for (int j = 0; j < 10; j++)
                            {

                                for (int z = 0; z < 10; z++)
                                {
                                    if (BlockSnatch.instance.gameSlots[j].slots[z].empty)
                                    {

                                        int ind = BlockParentblocks.IndexOf(currentBlock);
                                        BlockSnatch.instance.currentBlockList[ind].transform.localScale = Vector3.one;
                                        Vector3 savedPos = currentBlock.transform.position;
                                        currentBlock.transform.position = BlockSnatch.instance.gameSlots[j].slots[z].transform.position;
                                        int k = 0;
                                        if (currentBlock.units.Length == 0)
                                        {

                                            currentBlock.units = currentBlock.GetComponentsInChildren<BlockUnit>();
                                            print("New count of units" + currentBlock.units.Length);
                                        }

                                        foreach (BlockUnit unit in currentBlock.units)
                                        {
                                            int indexX = Mathf.RoundToInt(unit.transform.position.x);
                                            int indexY = Mathf.RoundToInt(unit.transform.position.y);
                                            if (indexX < 10 && indexY < 10)
                                            {
                                                if (indexX >= 0 && indexY >= 0)
                                                {

                                                    if (BlockSnatch.instance.gameSlots[indexX].slots[indexY].empty)
                                                    {
                                                        k++;
                                                    }

                                                }
                                            }
                                        }

                                        int index = BlockParentblocks.IndexOf(currentBlock);
                                        currentBlock.transform.position = savedPos;
                                        currentBlock.parentTransform.localScale = startScale;
                                        if (k == currentBlock.units.Length)
                                        {
                                            isGameOver = false;
                                            goto gameOverCheck;
                                        }
                                        else
                                        {
                                            isGameOver = true;
                                        }

                                    }
                                }
                            }
                        }
                    }
                gameOverCheck:
                    print("Check is Over");
                }
                if (isGameOver)
                {
                    BlockSnatch.instance.GameOver();
                }


            }




        }

    }

    IEnumerator ScaleFunction(Transform objectTransform,Vector3 desiredScale,Vector3 currentScale,float ScaleDuration)
    {
        float currentTime = 0f;
         if(desiredScale==Vector3.zero)
         {
            objectTransform.localScale = new Vector3(1.5f, 1.5f, 0f);
            yield return new WaitForEndOfFrame();
            while (currentTime <= ScaleDuration)
            {
                objectTransform.localScale = Vector3.Lerp(currentScale, desiredScale, currentTime / ScaleDuration);
                currentTime += Time.deltaTime;
                yield return null;
            }
            objectTransform.localScale = desiredScale;

        }
         else
         {
            while (currentTime <= ScaleDuration)
            {

                objectTransform.localScale = Vector3.Lerp(currentScale, desiredScale, currentTime / ScaleDuration);
                currentTime += Time.deltaTime;

                yield return null;
            }
            objectTransform.localScale = desiredScale;
         }
            

    }
   IEnumerator MoveFunction(Transform objectTransform, Vector3 desiredPos, Vector3 currentPos, float movementDuration)
    {
        float currentTime = 0f;

        while (currentTime <= movementDuration)
        {

            objectTransform.position = Vector3.Lerp(currentPos, desiredPos, currentTime / movementDuration);
            currentTime += Time.deltaTime;

            yield return null;
        }


    }




      void IDragHandler.OnDrag(PointerEventData eventData)
  //void OnMouseDrag()
    {
        if(parentTransform.localScale!=Vector3.one)
        {
            parentTransform.localScale = Vector3.one;
        }
        if (draggable)
        {

            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePos.x, mousePos.y+ yPosOffset, 0f);
            markerOnSlots = new List<Slot>();
            foreach (BlockUnit unit in units)
            {

                int indexX = Mathf.RoundToInt(unit.transform.position.x);
                int indexY = Mathf.RoundToInt(unit.transform.position.y);
                if (indexX < 10 && indexY < 10)
                {
                    if (indexX >= 0 && indexY >= 0)
                    {

                        for (int j = 0; j < 10; j++)
                        {

                            for (int z = 0; z < 10; z++)
                            {
                                if (j == indexX && z == indexY)
                                {
                                    markerOnSlots.Add(BlockSnatch.instance.gameSlots[j].slots[z]);
                                }
                            }
                        }
                    }
                }




            }

            for (int j = 0; j < 10; j++)
            {

                for (int z = 0; z < 10; z++)
                {

                    if (markerOnSlots.Contains(BlockSnatch.instance.gameSlots[j].slots[z]))
                    {
                        if (BlockSnatch.instance.gameSlots[j].slots[z].empty)
                        {
                            BlockSnatch.instance.gameSlots[j].slots[z].MarkerObject.SetActive(true);

                        }

                    }
                    else
                    {
                        BlockSnatch.instance.gameSlots[j].slots[z].MarkerObject.SetActive(false);
                    }
                }

            }
        }



    }
}
