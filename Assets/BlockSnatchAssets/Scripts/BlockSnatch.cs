﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using System.IO;
using Strobotnik.GUA;
using GoogleMobileAds.Api;
using UnityEngine.UI;

public class BlockSnatch : MonoBehaviour
{
    public static BlockSnatch instance;
    public int gridSize=10;
    public List<GridColumn> gameSlots = new List<GridColumn>();
    public List<GameObject> blockSlots = new List<GameObject>();
    public List<GameObject> allBlocks = new List<GameObject>();
    public List<Slot> allSlot = new List<Slot>();
    public List<AudioSource> audioSources = new List<AudioSource>();
    public GameObject slotPrefab;
    public int blocksCount=3;
    public List<GameObject> BlockVariants = new List<GameObject>();
    public List<GameObject> currentBlockList ;
    public GameObject gameOverPanel;
    public List<Sprite> ColourSprites = new List<Sprite>();
    public bool blocksSpawned;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI finalScoreText;
    public int score;
    public GameObject homeScreen;
    public GameObject grid;
    public Vector3 cameraPos=new Vector3(4.5f,2.7f,-10f);
    public bool isGooglePlayStoreVersion;
    string devGameName;
    string gameTitle;
    string paytmGamesLink;
    SimpleJSON.JSONNode mainJsonData;
    GameDownloader gameDownloader;
    public System.Reflection.Assembly assembly;
    public SimpleJSON.JSONNode gameValuesData;
    public TextMeshProUGUI SnatchCountDigit;
    public TextMeshProUGUI SnatchCountDigitShadow;
    public GameObject multiplierGameobject;
    [HideInInspector]
    public AssetBundle mainAssetBundle = null;


    string mainJsonDataText;

    string scriptFilePath;

    string gameFolderName = "";

    bool analyticsSetupDone = false;

    GameObject analyticsGameobject;
    int currentSoundState;
    string soundStringPrefs;
    public Button playButton;
    public Button restartButton;
    public Button homeButton;
    public Button soundButton;
    public Button infoButton;
    public Button tutorialButton;
    public bool volumeStatus=true;
    public AudioListener audioListener;
    public Sprite soundOn;
    public Sprite soundOff;
    public Image volumeIcon;

    public System.Type BlockParent;
    public System.Type Slot;
    public System.Type BlockUnit;
    GameObject BlockSnatchPrefab;
    public TextAsset localJsonGameValuesText;
    string gameValuesVersionPrefs = "";
    string gameValuesFileName = "";
    public int points;
    public AudioClip gameOverSFX;
    public AudioClip Bgm;
    public AudioClip pickingSFX;
    public AudioClip placingSFX;
    public AudioClip incorrectPlacementSFX;
    public AudioClip rowCompletedSFX;
    public float horizontalResolution = 1920;
    public SpriteRenderer BgRenderer;
    public List<GameObject> tutorialImages;
    public GameObject tutorialWindow;
    int tutorialImageCounter=0;
    private void Awake()
    {
        instance = this;
    }


    IEnumerator Start()
    {
        SetScreenOrientation();
        
        isGooglePlayStoreVersion = false;

        devGameName = "BlockSnatch";

        gameTitle = "Block Snatch";

        paytmGamesLink = "https://paytmfirstgames.com/";


        GetMainAssetBundleAndMainJsonData();
        SetStringsFromMainJsonAndLoadPrefs();

        GetReferences();

        AddAudioSources();
        AddListeners();

        GetCurrentSoundStateFromPrefs();

        SetMaterialShaders();

        Application.targetFrameRate = 60;
        if(isGooglePlayStoreVersion)
        {
            LoadLocalGameValues();
        }
        if (mainJsonData!=null)
        {
            print("paytm Version");
            StartCoroutine(GetGameValues());
            yield return new WaitForSeconds(1f);

        }
       
        
        StartGame();
        string loadingBarAmountPrefsText = "LoadingBarFillAmount";
        PlayerPrefs.SetFloat(devGameName + loadingBarAmountPrefsText, 1f);

        if (gameDownloader != null)
        {
            gameDownloader.SetTargetLoadingBarImageAmount(1);
        }

        if (isGooglePlayStoreVersion)
        {
            InitializeMobileAds();
        }
        yield return null;
    }
    void AddAudioSources()
    {
        GameObject AudioSourceParent = new GameObject();
        AudioSourceParent.name = "AudioSourceParent";
        for (int count = 0; count < 10; count++)
        {
            GameObject audioSource = new GameObject("audioSource" + count);
            audioSources.Add(audioSource.AddComponent<AudioSource>());
            audioSource.transform.SetParent(AudioSourceParent.transform);
            audioSources.Add(audioSource.GetComponent<AudioSource>());
        }

    }
    public void SetScreenOrientation()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }
    public void GetMainAssetBundleAndMainJsonData()
    {
        mainJsonData = null;
        isGooglePlayStoreVersion = false;

        gameDownloader = GameDownloader.instance;
        GameObject abdObj = null;
        if (gameDownloader == null)
        {
            string abdString = "GameDownloader";
            abdObj = GameObject.Find(abdString);
        }
        else
        {
            abdObj = gameDownloader.gameObject;
        }
        if (abdObj != null)
        {
            if (abdObj.GetComponent<GameDownloader>())
            {
                if (gameDownloader == null)
                {
                    gameDownloader = abdObj.GetComponent<GameDownloader>();
                }
            }
            if (gameDownloader == null)
            {
                Debug.Log("GameDownloader component not found");
            }
            else
            {
                mainAssetBundle = gameDownloader.GetMainAssetBundle();
                mainJsonData = gameDownloader.GetMainJsonData();
                if (gameDownloader.isTargetGooglePlayStore())
                {
                    isGooglePlayStoreVersion = true;
                }
            }
        }
        else
        {
            Debug.Log("GameDownloader Gameobject not found");
        }

        if (mainAssetBundle == null)
        {
            string assetBundleFilePath = PlayerPrefs.GetString(devGameName + "assetBundleFilePath");
            Debug.Log("mainAssetBundle " + assetBundleFilePath);
            if (File.Exists(assetBundleFilePath))
            {
                mainAssetBundle = AssetBundle.LoadFromFile(assetBundleFilePath);
            }
        }

        if (mainJsonData == null)
        {
            mainJsonDataText = PlayerPrefs.GetString(devGameName + "mainJsonDataText");
            print("mainJsonDataText");
            print(mainJsonDataText);
            if (mainJsonDataText.Length > 10)
            {
                mainJsonData = SimpleJSON.JSON.Parse(mainJsonDataText);
            }
            print("mainJsonData");
            print(mainJsonData);
        }

        scriptFilePath = PlayerPrefs.GetString(devGameName + "scriptFilePath");
    }

    public void BgImageScaler()
    {
        Camera.main.transform.position = cameraPos;
        Camera.main.orthographicSize = 9.75f;
        float screenRatio = (float)Screen.width / (float)Screen.height;
        float targetRatio = BgRenderer.bounds.size.x / BgRenderer.bounds.size.y;

        if (screenRatio >= targetRatio)
        {
            Camera.main.orthographicSize = BgRenderer.bounds.size.y / 2;
        }
        else
        {
            float differenceInSize = targetRatio / screenRatio;
            Camera.main.orthographicSize = BgRenderer.bounds.size.y / 2 * differenceInSize;
        }

          float screenHeight = Camera.main.orthographicSize * 2f;
          float screenWidth = screenHeight / Screen.height * Screen.width;
          float width = screenWidth / BgRenderer.sprite.bounds.size.x;
          float height = screenHeight / BgRenderer.sprite.bounds.size.y;
          BgRenderer.transform.localScale = new Vector3(width, height, 1f);
          


      
    }



    public void SetStringsFromMainJsonAndLoadPrefs()
    {
        if (mainJsonData != null)
        {
            gameTitle = mainJsonData["mainData"]["gameTitle"];
            devGameName = mainJsonData["mainData"]["devGameName"];
        }

        gameFolderName = devGameName + "Data";
        gameValuesFileName = devGameName + "GameValues.json";


        soundStringPrefs = devGameName + "Sounds";
        gameValuesVersionPrefs = devGameName + "GameValuesVersion";
    }

    IEnumerator GetGameValues()
    {
        int gameValuesVersion;
        string url = mainJsonData["mainData"]["gameValuesUrl"];
        SimpleJSON.JSONNode newGameValuesJsonData;
        string filePath = Application.persistentDataPath + "/" + gameFolderName + "/" + gameValuesFileName;
        if (url.Length > 1)
        {
            print("Downloaded GameValues");
            print(url);
            WWW www = new WWW(url);
            yield return www;
            if (www.error == null)
            {
                print(www.text);
                newGameValuesJsonData = SimpleJSON.JSON.Parse(www.text);
                print(newGameValuesJsonData);
                gameValuesVersion = newGameValuesJsonData["Info"]["Version"];
                string val = newGameValuesJsonData["Info"]["Type"];
                gameValuesData = newGameValuesJsonData[val];
                print(gameValuesData["ScoreValues"]);
                points = gameValuesData["ScoreValues"];
                if (gameValuesVersion > PlayerPrefs.GetInt(gameValuesVersionPrefs))
                {
                 
                    string folderPath = Application.persistentDataPath + "/" + gameFolderName;
                    filePath = folderPath + "/" + gameValuesFileName;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    if (!File.Exists(filePath))
                    {
                        File.Create(filePath).Close();
                    }
                    File.WriteAllText(filePath, www.text);
                    PlayerPrefs.SetInt(gameValuesVersionPrefs, gameValuesVersion);
                }
            }
        }
    }



    public void GetReferences()
    {
        Camera.main.gameObject.AddComponent<Physics2DRaycaster>();
        analyticsSetupDone = false;
        bool useLocalGamePrefab = false;
        audioListener = Camera.main.GetComponent<AudioListener>();
        if (gameDownloader != null)
        {
            useLocalGamePrefab = gameDownloader.useLocalGamePrefab;
        }

        if (mainAssetBundle != null)
        {
            if (isGooglePlayStoreVersion)
            {
                BlockSnatchPrefab = this.gameObject;

            }
            else
            {
                BlockSnatchPrefab = Instantiate(mainAssetBundle.LoadAsset<GameObject>("BlockSnatchGame"));

            }

            blockSlots.Clear();
            BlockVariants.Clear();
            ColourSprites.Clear();
            blockSlots.Add(BlockSnatchPrefab.transform.GetChild(3).GetChild(0).gameObject);
            blockSlots.Add(BlockSnatchPrefab.transform.GetChild(3).GetChild(1).gameObject);
            blockSlots.Add(BlockSnatchPrefab.transform.GetChild(3).GetChild(2).gameObject);
            slotPrefab = mainAssetBundle.LoadAsset<GameObject>("BaseTile");
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent1"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent2"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent3"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent4"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent5"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent6"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent7"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent8"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent9"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent10"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent11"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent12"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent13"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent14"));
            BlockVariants.Add(mainAssetBundle.LoadAsset<GameObject>("BlockVariantParent15"));
            gameOverPanel = BlockSnatchPrefab.transform.GetChild(2).GetChild(2).gameObject;
            ColourSprites.Add(mainAssetBundle.LoadAsset<Sprite>("blue"));
            ColourSprites.Add(mainAssetBundle.LoadAsset<Sprite>("green"));
            ColourSprites.Add(mainAssetBundle.LoadAsset<Sprite>("purple"));
            ColourSprites.Add(mainAssetBundle.LoadAsset<Sprite>("red"));
            ColourSprites.Add(mainAssetBundle.LoadAsset<Sprite>("yellow"));
            soundOn = mainAssetBundle.LoadAsset<Sprite>("sound on");
            soundOff = mainAssetBundle.LoadAsset<Sprite>("sound off");
           // Sprite TutorialImage1=




            localJsonGameValuesText = mainAssetBundle.LoadAsset<TextAsset>("BlockSnatchGameValues.json");
            scoreText = BlockSnatchPrefab.transform.GetChild(2).GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
            finalScoreText = BlockSnatchPrefab.transform.GetChild(2).GetChild(2).GetChild(2).gameObject.GetComponent<TextMeshProUGUI>();
            homeScreen = BlockSnatchPrefab.transform.GetChild(2).GetChild(3).gameObject;
            grid = BlockSnatchPrefab.transform.GetChild(0).gameObject;
            volumeIcon = BlockSnatchPrefab.transform.GetChild(2).GetChild(3).GetChild(3).GetComponent<Image>();
            playButton = BlockSnatchPrefab.transform.GetChild(2).GetChild(3).GetChild(1).GetComponent<Button>();
            homeButton = BlockSnatchPrefab.transform.GetChild(2).GetChild(2).GetChild(3).GetComponent<Button>();
            restartButton = BlockSnatchPrefab.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Button>();
            soundButton = BlockSnatchPrefab.transform.GetChild(2).GetChild(3).GetChild(3).GetComponent<Button>();
            infoButton = BlockSnatchPrefab.transform.GetChild(2).GetChild(3).GetChild(2).GetComponent<Button>();
            multiplierGameobject = BlockSnatchPrefab.transform.GetChild(2).GetChild(1).gameObject;
            SnatchCountDigit = BlockSnatchPrefab.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();
            SnatchCountDigitShadow = BlockSnatchPrefab.transform.GetChild(2).GetChild(1).GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();
            tutorialWindow = BlockSnatchPrefab.transform.GetChild(2).GetChild(4).gameObject;
            tutorialButton = BlockSnatchPrefab.transform.GetChild(2).GetChild(4).gameObject.GetComponent<Button>();
            tutorialImages.Add( BlockSnatchPrefab.transform.GetChild(2).GetChild(4).GetChild(0).gameObject);
            tutorialImages.Add(BlockSnatchPrefab.transform.GetChild(2).GetChild(4).GetChild(1).gameObject);
            tutorialImages.Add(BlockSnatchPrefab.transform.GetChild(2).GetChild(4).GetChild(2).gameObject);
            gameOverSFX = mainAssetBundle.LoadAsset<AudioClip>("GameOver");
            pickingSFX = mainAssetBundle.LoadAsset<AudioClip>("PickSound");
            placingSFX = mainAssetBundle.LoadAsset<AudioClip>("PlacingSound");
            incorrectPlacementSFX = mainAssetBundle.LoadAsset<AudioClip>("IncorrectPlacement");
            rowCompletedSFX = mainAssetBundle.LoadAsset<AudioClip>("CompletedRow");

            if (isGooglePlayStoreVersion == false)
            {
                if (File.Exists(scriptFilePath))
                {

                    assembly = System.Reflection.Assembly.LoadFrom(scriptFilePath);
                    Slot = assembly.GetType("Slot");
                    BlockUnit = assembly.GetType("BlockUnit");
                    BlockParent = assembly.GetType("BlockParent");
                    if (BlockParent == null)
                    {
                        print("BlockParent is nul.l");
                    }
                    slotPrefab.AddComponent<Slot>();

                    for (int i = 0; i < BlockVariants.Count; i++)
                    {
                        BlockVariants[i].transform.GetChild(0).gameObject.AddComponent<BlockParent>();
                        SpriteRenderer[] units = BlockVariants[i].GetComponentsInChildren<SpriteRenderer>();
                        for (int j = 0; j < units.Length; j++)
                        {
                            units[j].gameObject.AddComponent<BlockUnit>();
                        }
                    }


                }
            }
    

        }
        BgRenderer = BlockSnatchPrefab.transform.GetChild(1).GetComponent<SpriteRenderer>();
        BgImageScaler();






        if (analyticsGameobject == null)
        {
            if (isGooglePlayStoreVersion)
            {
                if (analyticsGameobject == null)
                {
                    analyticsGameobject = new GameObject("Analytics");
                    analyticsGameobject.SetActive(false);
                    analyticsGameobject.AddComponent<Analytics>().enabled = false;
                }
                if (mainJsonData != null)
                {
                    string trackingId = "";

                    if (mainJsonData["mainData"]["analyticsTrackingID"])
                    {
                        trackingId = mainJsonData["mainData"]["analyticsTrackingID"];
                    }


                    string appName = gameTitle;

                    if (mainJsonData["mainData"]["analyticsAppName"])
                    {
                        appName = mainJsonData["mainData"]["analyticsAppName"];
                    }

                    if (analyticsGameobject.GetComponent<Analytics>())
                    {
                        analyticsGameobject.GetComponent<Analytics>().trackingID = trackingId;
                        analyticsGameobject.GetComponent<Analytics>().appName = appName;
                        analyticsGameobject.GetComponent<Analytics>().enabled = true;
                        analyticsGameobject.SetActive(true);
                        analyticsSetupDone = true;
                    }
                }
            }
        }


      
    }
    public void SetMaterialShaders()
    {
        if(!isGooglePlayStoreVersion)
        {
            List<TextMeshProUGUI> allTexts = new List<TextMeshProUGUI>();
            allTexts.Add(scoreText);
            allTexts.Add(finalScoreText);


            Shader textShader = Shader.Find("TextMeshPro/Distance Field");
            for (int count = 0; count < allTexts.Count; count++)
            {
                allTexts[count].materialForRendering.shader = textShader;
            }
        }
       
    }

    private void AddListeners()
    {

        print("add listeners  called");
        playButton.onClick.AddListener(OnPlayButtonClicked);
        print(playButton.name);
        homeButton.onClick.AddListener(HomeScreen);
        restartButton.onClick.AddListener(Restart);
        soundButton.onClick.AddListener(volumeFunctions);
        infoButton.onClick.AddListener(InfoWindowActivator);
        tutorialButton.onClick.AddListener(tutorialImageChanger);
    }

    public void InfoWindowActivator()
    {
        tutorialWindow.SetActive(true);
      
    }

    public void tutorialImageChanger()
    {
        foreach (GameObject imageT in tutorialImages)
        {
            imageT.SetActive(false);
        }
        if (tutorialImageCounter == tutorialImages.Count - 1)
        {
            tutorialWindow.SetActive(false);
            tutorialImages[0].SetActive(true);
            tutorialImageCounter = 0;
        }
        else
        {
            tutorialImageCounter += 1;
            tutorialImages[tutorialImageCounter].SetActive(true);
        }


    }

    public void GetCurrentSoundStateFromPrefs()
    {
        currentSoundState = PlayerPrefs.GetInt(soundStringPrefs);
    }

    public void LoadLocalGameValues()
    {
        int gameValuesVersion;
        string val;
        string jsonGameValuesTest = localJsonGameValuesText.text;
        SimpleJSON.JSONNode gameValuesJsonData;
        gameValuesJsonData = SimpleJSON.JSON.Parse(jsonGameValuesTest);
        gameValuesVersion = gameValuesJsonData["Info"]["Version"];
        val = gameValuesJsonData["Info"]["Type"];
        gameValuesData = gameValuesJsonData[val];

      
        print(gameValuesData["ScoreValues"]);
        points = gameValuesData["ScoreValues"];
        if (PlayerPrefs.GetInt(gameValuesVersionPrefs) < gameValuesVersion)
        {
            PlayerPrefs.SetInt(gameValuesVersionPrefs, gameValuesVersion);
        }

        string filePath = Application.persistentDataPath + "/" + gameFolderName + "/" + gameValuesFileName;
        if (File.Exists(filePath))
        {
            string gameValuesText = File.ReadAllText(filePath);
            SimpleJSON.JSONNode newGameValuesJsonData = SimpleJSON.JSON.Parse(gameValuesText);
            gameValuesVersion = newGameValuesJsonData["Info"]["Version"];
            if (gameValuesVersion > PlayerPrefs.GetInt(gameValuesVersionPrefs))
            {
                val = newGameValuesJsonData["Info"]["Type"];
                gameValuesData = newGameValuesJsonData[val];
                PlayerPrefs.SetInt(gameValuesVersionPrefs, gameValuesVersion);
            }
            
        }

    }


     public void InitializeMobileAds()
     {
         string appId = "ca-app-pub-3940256099942544~3347511713";
         if (mainJsonData != null)
         {
             if (mainJsonData["mainData"]["admobAppId"])
             {
                 appId = mainJsonData["mainData"]["admobAppId"];

             }
         }
         MobileAds.Initialize(appId);
         this.rewardBasedVideo = RewardBasedVideoAd.Instance;
         rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
         RequestRewardBasedVideo();

     }

     private RewardBasedVideoAd rewardBasedVideo;

     int deathAdInterval = 3;
     int numOfDeaths = 0;
     private void RequestRewardBasedVideo()
     {
         string adUnitId = "ca-app-pub-3940256099942544/5224354917";
         if (mainJsonData != null)
         {
             if (mainJsonData["mainData"]["admobAdId"])
             {
                 adUnitId = mainJsonData["mainData"]["admobAdId"];
             }
             if (mainJsonData["mainData"]["deathAdInterval"])
             {
                 deathAdInterval = mainJsonData["mainData"]["deathAdInterval"];
             }
         }
         // Create an empty ad request.
         AdRequest request = new AdRequest.Builder().Build();
         // Load the rewarded video ad with the request.
         this.rewardBasedVideo.LoadAd(request, adUnitId);
     }

     public void HandleRewardBasedVideoClosed(object sender, System.EventArgs args)
     {
         this.RequestRewardBasedVideo();
     }

     private void ShowVideoAd()
     {
         if (rewardBasedVideo.IsLoaded())
         {
             rewardBasedVideo.Show();
         }
     }
      

    private void StartGame()
    {

        {
            GridColumn[] gameSlotsArray;
            gameSlotsArray = new GridColumn[gridSize];
            for (int x = 0; x < gridSize; x += 1)
            {
                gameSlotsArray[x] = new GridColumn();
                gameSlotsArray[x].slots = new Slot[gridSize];
                for (int y = 0; y < gridSize; y += 1)
                {
                    GameObject slotObject = Instantiate(slotPrefab, new Vector3(x, y, 0), Quaternion.identity, grid.transform);
                    gameSlotsArray[x].slots[y] = slotObject.GetComponent<Slot>();
                    allSlot.Add(gameSlotsArray[x].slots[y]);
                }
            }
            gameSlots.AddRange(gameSlotsArray);
        }
        BlockSpawner();
    }

    public void Restart()
    {
        score = 0;
        scoreText.text = score.ToString();
        gameOverPanel.SetActive(false);
        BlockSpawner();
        blocksCount = 3;
        
    }

    public void HomeScreen()
    {
        homeScreen.SetActive(true);
        foreach (AudioSource source in audioSources)
        {
            if (!source.isPlaying)
            {
                source.clip = Bgm;
                source.loop = true;
                source.Play();
                
                break;
            }
        }
        Restart();
    }

    public void volumeFunctions()
    {
        volumeStatus = !volumeStatus;
        if(volumeStatus)
        {
            foreach (AudioSource source in audioSources)
            {
                source.mute = false;
            }
            volumeIcon.sprite = soundOn;

        }
        else
        {
            foreach (AudioSource source in audioSources)
            {
                source.mute = true;
            }
            volumeIcon.sprite = soundOff;
        }
    }

    public void BlockSpawner()
    {
        currentBlockList = new List<GameObject>();
        List<GameObject> selectedBlocks = new List<GameObject>();
        List<Sprite>selectedSprites= new List<Sprite>();
        List<GameObject> tmpListBlocks = new List<GameObject>(BlockVariants);
        List<Sprite> tmpListSprites = new List<Sprite>(ColourSprites);
        while (selectedBlocks.Count < 3 && tmpListBlocks.Count > 0)
        {
            int index = Random.Range(0, tmpListBlocks.Count);
            selectedBlocks.Add(tmpListBlocks[index]);
            tmpListBlocks.RemoveAt(index);
            int colourIndex = Random.Range(0, tmpListSprites.Count);
            selectedSprites.Add(tmpListSprites[colourIndex]);
            tmpListSprites.RemoveAt(colourIndex);
        }

        for (int i=0;i<3;i++)
        {
            GameObject blockParent= Instantiate(selectedBlocks[i],blockSlots[i].transform);
            allBlocks.Add(blockParent);
            SpriteRenderer[] colourSprites = blockParent.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer renderer in colourSprites)
            {
                renderer.sprite = selectedSprites[i];
            }
            currentBlockList.Add(blockParent);
        }
        blocksSpawned = true;
    }

    public void scoreUpdater(int rowCount)
    {
       if(rowCount>1)
        {
            StartCoroutine(SnatchRowCountActivator());
        }
        score += (points* rowCount)*rowCount;
        scoreText.text = score.ToString();

    }
    
    IEnumerator SnatchRowCountActivator()
    {
        multiplierGameobject.SetActive(true);
        RectTransform rectTransform = multiplierGameobject.GetComponent<RectTransform>();

       /* float currentTime = 0f;
        float movementDuration = 1f;
        while (currentTime <= movementDuration)
        {
            rectTransform.anchoredPosition = Vector2.Lerp(Vector2.zero, new Vector2(0f,125f), currentTime / movementDuration);
            currentTime += Time.deltaTime;
            yield return null;
        }*/
        

        yield return new WaitForSeconds(1f);
        multiplierGameobject.SetActive(false);
    }

    public void OnPlayButtonClicked()
    {
        homeScreen.SetActive(false);
        print(homeScreen.name);
        foreach (AudioSource source in audioSources)
        {
            source.Stop();
        }
    }

    public void GameOver()
    {
        finalScoreText.text = score.ToString();
        
        gameOverPanel.SetActive(true);
        foreach(AudioSource source in audioSources)
        {
            if(!source.isPlaying)
            {
                source.PlayOneShot(gameOverSFX);
                break;
            }
        }
        foreach(GameObject block in allBlocks)
        {
            Destroy(block);
        }
        foreach(Slot slot in allSlot)
        {
            slot.empty = true;
        }
        allBlocks = new List<GameObject>();
        HandleDeathVideoAd();

    }
    public void HandleDeathVideoAd()
    {
        numOfDeaths++;
        if (numOfDeaths % deathAdInterval == 0)
        {
            if (isGooglePlayStoreVersion)
            {
                ShowVideoAd();
            }
        }
    }
}
[System.Serializable]
public class GridColumn
{
    [SerializeField]
    public Slot[] slots;
}